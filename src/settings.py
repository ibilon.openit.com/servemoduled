import rootdir

port      = 20097
bucket    = 'www-openit-com-tokyo'
directory = 'modules'
prefix    = 'servemoduled'
root      = rootdir.get()
etc       = root / 'etc'
var       = root / 'var' / prefix
static    = root / 'var' / 'static'
modules   = static / prefix / 'modules'
etag      = var / 'etag'
apifile   = etc / 'api'
api       = apifile.read_text().strip()
