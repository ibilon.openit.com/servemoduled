import settings
import uvicorn
import restapi

app = restapi.app

def main():
    configure()
    serve()

def configure():
    mkdir(settings.modules)
    mkdir(settings.etag)

def mkdir(path):
    return path.mkdir(parents=True, exist_ok=True)

def serve():
    if __name__ == '__main__':
        uvicorn.run('main:app', port=settings.port, use_colors=False)

try:
    main()
except KeyboardInterrupt:
    pass
