from fastapi.responses import RedirectResponse
from fastapi import FastAPI, HTTPException
import objectstore
import botocore
import settings

app = FastAPI()

@app.head('/channels/{channel}/operating-systems/{os}/architectures/{arch}/modules/{module_name}')
@app.get('/channels/{channel}/operating-systems/{os}/architectures/{arch}/modules/{module_name}')
async def get_module(channel: str, os: str, arch: str, module_name: str):
    try:
        object_name = f'{module_name}-{os}-{arch}.tar.gz'
        result      = await objectstore.get(object_name, channel)
        location    = f'{settings.api}/services/static/{settings.prefix}/modules/{channel}/{object_name}'
        return RedirectResponse(location)
    except botocore.exceptions.ClientError as e:
        code = e.response['Error']['Code']
        if code == 'AccessDenied':
            raise HTTPException(status_code=404)
