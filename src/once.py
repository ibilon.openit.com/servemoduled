import asyncio

dictionaries = dict()

def once(function):
    if not function in dictionaries:
        dictionaries[function] = dict()
    dictionary = dictionaries[function]
    async def wrapper(*args):
        id        = hash(args)
        result    = None
        exception = None
        if not id in dictionary or dictionary[id] == None:
            queues = []
            dictionary[id] = queues
            try:
                result = await function(*args)
            except Exception as e:
                exception = e
            dictionary[id] = None
            for queue in queues:
                await queue.put((result, exception))
        else:
            queue = asyncio.Queue()
            dictionary[id].append(queue)
            result, exception = await queue.get()
        if exception:
            raise exception
        return result
    return wrapper
