from aiobotocore.session import get_session
from botocore.config import Config
from botocore import UNSIGNED
from once import once
import settings

session = get_session()

@once
async def get(object_name, channel):
    key = f'{settings.directory}/{channel}/{object_name}'
    async with create_client() as client:
        filepath = settings.modules / channel / object_name
        response = await client.get_object(Bucket=settings.bucket, Key=key)
        body     = response['Body']
        etag     = response['ResponseMetadata']['HTTPHeaders']['etag']
        if not filepath.parent.exists():
            filepath.parent.mkdir()
        if not filepath.exists() or etag != get_etag(object_name, channel):
            async with body as stream:
                with open(filepath, 'wb') as file:
                    async for buffer in stream.iter_chunked(4096):
                        file.write(buffer)
            save_etag(object_name, channel, etag)
        return {'filepath': filepath, 'etag': etag}

def create_client():
    return session.create_client('s3', config=Config(signature_version=UNSIGNED))

def get_etag(object_name, channel):
    filepath = settings.etag / channel / object_name
    if filepath.exists():
        with open(filepath, 'r') as file:
            return file.read()
    return None

def save_etag(object_name, channel, etag):
    filepath = settings.etag / channel / object_name
    if not filepath.parent.exists():
        filepath.parent.mkdir()
    with open(filepath, 'w') as file:
        file.write(etag)
