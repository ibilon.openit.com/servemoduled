@echo off
set root=%~dp0\..
cd "%root%"
rmdir /s /q dist
rmdir /s /q build
del /s /q *.spec
python -m pip install -r requirements.txt --upgrade
python -m PyInstaller --onefile "%root%\src\main.py" --name openit_servemoduled --icon "%root%\icon.ico" ^
    --hidden-import uvicorn.logging ^
    --hidden-import uvicorn.loops ^
    --hidden-import uvicorn.loops.auto ^
    --hidden-import uvicorn.protocols ^
    --hidden-import uvicorn.protocols.http ^
    --hidden-import uvicorn.protocols.http.auto ^
    --hidden-import uvicorn.protocols.websockets ^
    --hidden-import uvicorn.protocols.websockets.auto ^
    --hidden-import uvicorn.lifespan ^
    --hidden-import uvicorn.lifespan.on ^
    --hidden-import websockets.legacy ^
    --hidden-import websockets.legacy.server ^
    --hidden-import main
